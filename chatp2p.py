from socket import *
from sys import argv
from select import select
import sys
import re

reload(sys)
sys.setdefaultencoding('utf-8')


class Server:
	#socket server
	main_connection=socket(AF_INET, SOCK_STREAM)
	#list of 
	connections=[]
	client_to_read=[]
	list_ban=[]
	#list of ip adress connected
	list_ips=[]
	#list of client connected
	who_is=[]
	dico={}

	def found_key(self,value):
            for k,v in self.dico.items():
                if v==value:
                    return k

	def __init__(self):
            self.main_connection.bind(("0.0.0.0",1664))
            self.main_connection.listen(5)

	def run(self):
            server_run=True
            while server_run:
                requested_connections, wlist, xlist=select([self.main_connection],[], [], 0.05)
                for connection in requested_connections:
                    connection_with_client, infos_connection=connection.accept()
                    self.connections.append(connection_with_client)
                    self.list_ips.append(infos_connection[0])
                    print(str(infos_connection[0])," connected")

		    print("List connection %s"% self.connections)
		    print("List who is %s" % self.who_is)

                try:
                    client_to_read, wlist, xlist=select(self.connections,[], [], 0.05)
                except:
                    pass
                else:
                    for client in client_to_read:
                        msg_received=client.recv(1024)
                        msg_received=msg_received.decode()
                        nic=""
			#client who speek
                        to=client
                        msg=""
                    
                        if msg_received!="":
                            print(str(client)+"Recu %s: " % msg_received)

                        if msg_received[:4]=="2000" and msg_received[4]==" ":
                            nic=str(msg_received[5:])
                            self.who_is.append(nic)
                            self.HELLO(nic,to)

                        if msg_received=="?":
                            self.details(to)

                        if msg_received=="who":
                            self.who(to)


                        if msg_received[:2]=="pm" and msg_received[2]==" ":
                            ni=""
                            for i in range(3,100):
                                if msg_received[i]==" ":
                                    ni=i
                                    break
                            nic=str(msg_received[3:ni])
                            msg=msg_received[ni+1:]

                            if nic in self.who_is:
                                nic_sock=self.found_key(nic)
                                self.pm(nic_sock,msg,to)
                            else:
                                print("Aucun utilisateur connecte a ce surnom")
			    

                        if msg_received[:2]=="bm" and msg_received[2]==" ":
                            msg=msg_received[3:]
                            self.bm(msg,to)
 
                        if msg_received[:3]=="ban" and msg_received[3]==" ":
                            nic=str(msg_received[4:])
                            self.ban(nic,to)

                        if msg_received[:5]=="unban" and msg_received[5]==" ":
                            nic=str(msg_received[6:])
                            self.unban(nic,to)

                        if msg_received=="quit":
                            self.quit()
 
                        if msg_received=="see ban":
                            print(str(self.list_ban))

                        if msg_received=="see list ips":
                            print(str(self.list_ips))

                        if msg_received=="see dico":
                            print(str(self.dico))

                        if msg_received=="see connections":
                            print(str(self.connections))


        def HELLO(self,nic,to):
			to.send(("1000"+" "+"Bienvenue "+nic+"\n").encode())
			to.send(("5000"+" ").encode())
			self.dico[to]=nic
			for c in self.connections:
				c.send((nic+" est connecte(e)\n").encode())

        def IPS(self,list_ips,to):
            to.send(('"('+str(list_ips)[1:-1]+')"').encode())

        def details(self,to):
            details=("\n<who>: voir les utilisateurs connectes"
			+"\n<pm nic msg>: envoyer un message 'msg' prive a l'utilisateur 'nic'"
			+"\n<bm msg>: envoyer un message 'msg' a tout les utilisateurs connectes"
			+"\n<ban nic>: bannir l'utilisateur 'nic'"
			+"\n<unban nic>: sortir l'utilisateur 'nic' de la liste des bannis"
			+"\n<quit>: quitter le serveur\n")
            to.send(("1000"+" "+details).encode())
			
        def who(self,to):
			self.IPS(self.list_ips,to)
			to.send(("who"+" ").encode())
			for x in self.who_is:
				to.send((x+" ").encode())

        def pm(self,nic,msg,fromm):
            nic.send(("4000"+" "+self.dico.get(fromm)+" dit: "+msg+"\r\n").encode())

        def bm(self,msg,fromm):
            for c in self.connections:
        	c.send(("5000"+" "+"Nouveau message de "+self.dico.get(fromm)+": "+msg+"\r\n").encode())

        def ban(self,nic,to):
            if self.found_key(nic)!=to:
                if nic in self.who_is:
                    to.send(("1000"+" "+nic + " est banni(e)").encode())
                    (self.found_key(nic)).send(("Vous etes banni(e)\r\n").encode())
                    self.list_ban.append(nic)
                    self.connections.remove(self.found_key(nic))
                    self.who_is.remove(nic)
                elif nic in self.list_ban:
                    to.send(("1000"+" "+nic + " est deja banni(e)").encode())
                else:
                    to.send(("1000"+" "+"Cet utilisateur n'existe pas").encode())
            else:
                to.send(("1000"+" "+"Vous ne pouvez pas vous bannir!\r\n").encode())

	def unban(self,nic,to):
            if nic in self.list_ban:
                to.send(("1000"+" "+nic + " n'est plus banni(e)").encode())
                (self.found_key(nic)).send(("Vous n'etes plus banni(e)\r\n").encode())
                self.list_ban.remove(nic)
                self.connections.append(self.found_key(nic))
                self.who_is.append(nic)
            elif nic in self.connections:
                to.send(("1000"+" "+nic + " n'est pas banni(e)").encode())
            else:
                to.send(("1000"+" "+"Cet utilisateur n'existe pas").encode())

	def quit(self):		
		for client in self.connections:
			client.send(("1000"+" "+"Bye-Bye").encode())
			client.close()
			sys.exit()
		self.main_connection.close()
		sys.exit()


class Client:
	connection_client=socket(AF_INET, SOCK_STREAM)
	list_cli=[]

        def START(self,nic):
            self.connection_client.send("2000"+" "+(nic).encode())

	def sendBroadcastMsg(self):
            self.connection_client.send((raw_input("Commande(<?> pour voir les commandes possible): ")).encode())

	def __init__(self, adress):
		nickname=""

		try:
		    self.connection_client.connect((adress,1664))

		except:
		    print("La connection a echouee")
		    sys.exit()

		if nickname=="" :
			nickname=raw_input("Bonjour choisissez un nickname : ")
			if nickname in self.list_cli:
				print("Ce nickname existe deja !")
				nickname=raw_input("Bonjour choisissez un autre nickname : ")

		if nickname!="" :
			self.list_cli.append(nickname)
			self.START(nickname)

		while True:
			data=self.connection_client.recv(1024)
			data=data.decode()

			if data[:3]=="who" and data[3]==" ":
				print("===Who is online===")
				print(data[4:]+"\r\n")

			if data[:4]=="1000" and data[4]==" ":
				print("===System Broadcast===")
				print(data[5:])

			if data[:4]=="4000" and data[4]==" ":
				print("===Message prive recu===")
				print(data[5:])

			if data[:4]=="5000" and data[4]==" ":
				print("===Message Broadcast===")
				print(data[5:])

			self.sendBroadcastMsg()
	
			if not data:
				break

if (len(sys.argv) > 1):
	ipAdress=sys.argv[1]
	e=re.compile(r"[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+")
	corresp=e.match(ipAdress)
	if corresp is not None:
		client=Client(sys.argv[1])
	else:
		print("Adresse ip du serveur manquante ou incorrecte")
else:
	server=Server()
	server.run()
